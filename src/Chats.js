import React from "react";
import Chat from "./Chat";

function Chats() {
    return (
        <div className="chats">
            <Chat
                name="Mark"
                message="What's up"
                timestamp="40 seconds ago"
                profilePic="https://upload.wikimedia.org/wikipedia/commons/1/14/Mark_Zuckerberg_F8_2018_Keynote_%28cropped_2%29.jpg"
            />
            <Chat
                name="Ellen"
                message="Hey! How are you :)"
                timestamp="35 minutes ago ago"
                profilePic="https://thumbor.forbes.com/thumbor/fit-in/416x416/filters%3Aformat%28jpg%29/https%3A%2F%2Fspecials-images.forbesimg.com%2Fimageserve%2F5ed560d07fe4060006bbce1e%2F0x0.jpg%3Fbackground%3D000000%26cropX1%3D422%26cropX2%3D1300%26cropY1%3D0%26cropY2%3D879"
            />
            <Chat
                name="Sandra"
                message="Hello"
                timestamp="3 days ago"
                profilePic="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRGH7MgodIrdlhF0HlTMlJSKsbNhIv5dRjDcA&usqp=CAU"
            />
            <Chat
                name="Natasha"
                message="Oops there is he is..."
                timestamp="1 week ago"
                profilePic="https://m.media-amazon.com/images/M/MV5BMGJiMjMzY2ItNTFkOC00NTU1LTk1MmYtYmY2YzJiMDNjNzhkXkEyXkFqcGdeQXVyMTY4ODIxOTI@._V1_.jpg"
            />
        </div>
    )
}

export default Chats;