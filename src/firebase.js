import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyAp0qkoU8I4DCzxSHaNylMFbmgz80OGLTI",
    authDomain: "study-tinder-react.firebaseapp.com",
    databaseURL: "https://study-tinder-react.firebaseio.com",
    projectId: "study-tinder-react",
    storageBucket: "study-tinder-react.appspot.com",
    messagingSenderId: "407899581008",
    appId: "1:407899581008:web:0b4a5ea3b179578c267f55",
    measurementId: "G-L301BYX7NK",
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const database = firebaseApp.firestore();

export default database;